/**
 * Created by Basti on 01.01.2017.
 */
/**
 * Created by Basti on 17.12.2016.
 */
var app = angular.module('bi-assessment.feedBackCtrl', [])

    .controller('feedBackCtrl', ['$scope', '$q', '$http', '$state', '$translate', 'mwFormResponseUtils', 'databaseService', function ($scope, $q, $http, $state, $translate, mwFormResponseUtils, databaseService) {
        console.log("feedBackCtrl running!");
        $scope.start = false;

        // Umfrage-Design
        var feedBackSurvey = feedBackSurveyModel.model;
        //var fakeResponse = surveyModel.fakeResponse;

        // Settings for Survey
        var ctrl3 = this;
        $scope.ctrl3 = ctrl3;
        ctrl3.cmergeFormWithResponse = false;
        ctrl3.cgetQuestionWithResponseList = false;
        ctrl3.cgetResponseSheetHeaders = false;
        ctrl3.cgetResponseSheetRow = false;
        ctrl3.cgetResponseSheet = false;
        ctrl3.headersWithQuestionNumber = true;

        ctrl3.formData = feedBackSurvey;
        ctrl3.templateData = {};
        ctrl3.formStatus = {};
        ctrl3.formOptions = {autoStart: true};
        ctrl3.formViewer = {};
        ctrl3.responseData = {};
        ctrl3.viewerReadOnly = false;

        //How to use mwFormResponseUtils
        ctrl3.getMerged = function () {
            return mwFormResponseUtils.mergeFormWithResponse(ctrl3.formData, ctrl3.responseData);
        };
        ctrl3.getQuestionWithResponseList = function () {
            return mwFormResponseUtils.getQuestionWithResponseList(ctrl3.formData, ctrl3.responseData);
        };
        ctrl3.getResponseSheetRow = function () {
            return mwFormResponseUtils.getResponseSheetRow(ctrl3.formData, ctrl3.responseData);
        };
        ctrl3.getResponseSheetHeaders = function () {
            return mwFormResponseUtils.getResponseSheetHeaders(ctrl3.formData, ctrl3.headersWithQuestionNumber);
        };
        ctrl3.getResponseSheet = function () {
            return mwFormResponseUtils.getResponseSheet(ctrl3.formData, ctrl3.responseData, ctrl3.headersWithQuestionNumber);
        };

        $scope.startSurvey = function () {
            $scope.start = true;
        };



        // Save Response to Database
        ctrl3.saveResponse = function () {
            var response = ctrl3.getResponseSheet();
            //var response = surveyModel.fakeResponse[0]; // Response-Mock
            console.log("Erhaltene Antwort: ", response);
            var fragen = response[0];
            var antworten = response[1];
            $scope.alias = "feedback";

            var adjustedResponse = {};
            adjustedResponse["alias"] = "feedback";
            adjustedResponse["answers"] = JSON.stringify(response, null, 2);

            var evaluationMapping = {
                "unlikely": 0,
                "somewhat unlikely": 1,
                "neutral": 2,
                "somewhat likely": 3,
                "likely": 4
            };

            var feedBack1 = 0;
            fragen.forEach(function (frage, index) {
                if (frage.indexOf("recommend the tool to a colleague") !== -1) {
                    feedBack1 += evaluationMapping[antworten[index]];
                    //console.log("answer: ", [antworten[index]], "evaluationMapping: ", evaluationMapping[antworten[index]], "usefulness Score: ", $scope.usefulnessScore);
                }
            });

            var feedBack2 = 0;
            fragen.forEach(function (frage, index) {
                if (frage.indexOf("influence my choice of BI tools") !== -1) {
                    feedBack2 += evaluationMapping[antworten[index]];
                    //console.log("answer: ", [antworten[index]], "evaluationMapping: ", evaluationMapping[antworten[index]], "usefulness Score: ", $scope.usefulnessScore);
                }
            });

            var feedBack3 = "";
            fragen.forEach(function (frage, index) {
                if (frage.indexOf("General Comments") !== -1) {
                    feedBack3 = antworten[index];
                    console.log(feedBack3);
                }
            });


            feedBack1 = feedBack1/4;
            feedBack2 = feedBack2/4;
            adjustedResponse["feedBack1"] = feedBack1;
            adjustedResponse["feedBack2"] = feedBack2;
            adjustedResponse["feedBack3"] = feedBack3;


            console.log("Das sind die FeedBack Ergebnisse ", adjustedResponse);

            //save survey response to database
            databaseService.saveResponse(adjustedResponse).then(function (res) {
                console.log("Antwort gespeichert!", res);
                $state.go('results');
            });
        };

    }]);


