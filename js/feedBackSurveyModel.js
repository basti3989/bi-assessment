/**
 * Created by Basti on 01.01.2017.
 */
/**
 * Created by Basti on 17.12.2016.
 */
var feedBackSurveyModel = {
    "model": {
        "pages": [
            {
                "id": "4625fd3a9bea53745552553cb00962s3f8",
                "number": 1,
                "name": "Section 1. Questions about yourself and your Organization",
                "description": null,
                "pageFlow": {
                    "nextPage": true,
                    "label": "mwForm.pageFlow.goToNextPage"
                },
                "elements": [
                    {
                        "id": "1080b8d6f4e2105fs2a7f95d87f71921ab",
                        "orderNo": 1,
                        "type": "question",
                        "question": {
                            "id": "07a2d08c8162339b4d1588ebs42fe4c9dd",
                            "text": "Please tell us what you think about the BI assessment tool",
                            "type": "grid",
                            "required": false,
                            "grid": {
                                "rows": [
                                    {
                                        "id": "3f2d5562267ec44198bs25f30276e31c76",
                                        "orderNo": 1,
                                        "label": "I will recommend the tool to a colleague or friend"
                                    },
                                    {
                                        "id": "4b8562aaa988466abs2bsb531bf4a3ab948",
                                        "orderNo": 2,
                                        "label": "The tool will influence my choice of BI tools in the future"
                                    },

                                ],
                                "cols": [
                                    {
                                        "id": "1d7f5d1a0262c65bb9s5fbf3c819c1be06",
                                        "orderNo": 1,
                                        "label": "unlikely"
                                    },
                                    {
                                        "id": "e325cfa4d67909d595f1cs2f883091a7b0",
                                        "orderNo": 2,
                                        "label": "somewhat unlikely"
                                    },
                                    {
                                        "id": "5859bcbb7c6e88ba4sb15360ae10e2df51",
                                        "orderNo": 3,
                                        "label": "neutral"
                                    },
                                    {
                                        "id": "5f0a059d4d40887b525f06s5753044c23a",
                                        "orderNo": 4,
                                        "label": "somewhat likely"
                                    },
                                    {
                                        "id": "df2853bf7142435fs9967391abba013097",
                                        "orderNo": 5,
                                        "label": "likely"
                                    }
                                ],
                                "cellInputType": "radio"
                            },
                        }
                    },
                    {
                        "id": "87506187c771d5a92aad50f46552b223",
                        "orderNo": 1,
                        "type": "question",
                        "question": {
                            "id": "2798333eb5f4ad2625f01f49b47b9d40",
                            "text": "General Comments / Suggestions:",
                            "type": "textarea",
                            "required": false,
                            "pageFlowModifier": false}
                    }
                ],
                "namedPage": false
            }
        ],
        "confirmationMessage": "Sehen Sie hier ihre Ergebnisse ein: Link zu Ergebnissen"
    }
};

