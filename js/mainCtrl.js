var app = angular.module('bi-assessment.mainCtrl', [])

  .controller('mainCtrl', ['$scope', '$q', '$http', '$state', '$translate', '$timeout', 'mwFormResponseUtils', 'databaseService', 'csvService', function ($scope, $q, $http, $state, $translate, $timeout, mwFormResponseUtils, databaseService, csvService) {
      console.log("mainCtrl running!");
      $state.go('home');

      // CSV Import/Export
      $scope.config = surveyModel; // Kopfzeilen Variable
      $scope.csv = {
          content: null,
          header: true,
          headerVisible: true,
          separator: ';',
          separatorVisible: false,
          accept: ".csv",
          result: null,
          encoding: 'ISO-8859-1',
          encodingVisible: false,
          uploadButtonLabel: "upload a csv file"
      };

      // CSV-Export
      $scope.csvExport = function () {
          return csvService.csvExport();
      };
      // CSV-Import
      $scope.csvImport = function () {
          console.log("Method fired!", $scope.csv.result);
          csvService.csvImport($scope.csv.result).then(function () {
              window.alert("Import finished: " + $scope.csv.result.length + " Elemente gespeichert");
              $scope.csv.result = null;
          });
      };

      /*
       $scope.$watch("csvResult", function (res) {
       console.log("Watcher fired!", res);
       if (res.length === 0) {
       return;
       }
       $scope.csvImport();
       }, true);
       */

      // Delete Empty Entries DB
      $scope.deleteEmptyEntries = function () {
          $scope.allResults.forEach(function (result) {
              if (result.easeOfUse === null) {
                  databaseService.deleteById(result.id).then(function () {
                      alert("Empty element deleted!");
                  })
              }
          });
      };
      // Empty DB
      $scope.emptyDB = function () {
          databaseService.emptyDB().then(function () {
              window.alert("DB cleared!");
          })
      };

      $scope.scrollToTop = function () {
          window.scrollTo(0, 0);
      };

      // Download DB
      $scope.downloadDB = function () {
          return databaseService.saveDB();
      };

  }]);