var app = angular.module('bi-assessment.resultsCtrl', [])

  .controller('resultsCtrl', ['$scope', '$q', '$http', '$state', 'databaseService', function ($scope, $q, $http, $state, databaseService) {
      console.log("resultsCtrl running!");

      //####Variablen für Drop-Down Liste auf Result Page#####
      $scope.features = ["AdvancedVisualization", "BusinessQuery", "Calculations", "Collaboration", "Dashboards", "Drilling", "ETL", "InMemoryAnalysis", "InteractiveReports", "MobileBI", "PredictiveAnalysis", "ScheduledReporting", "Spreadsheet", "StatisticalMethods", "VisualDataDiscovery"];
      $scope.showfeatureByTool = true;
      $scope.categories = ['Overall', 'Usefulness', 'Ease Of Use', 'Perceived Benefits', 'Intention to Use'];
      $scope.tools = ["All", "Adaptive Insights Adaptive Suite", "Alteryx", "Antivia DecisionPoint", "arcplan Enterprise/Edge",
          "Birst", "Bissantz DeltaMaster", "BOARD", "CALUMO", "Chartio", "Corporate Planner", "Corporater EPM Suite",
          "Cubeware Cockpit", "cubus", "Cyberscience Cyberquery", "Dimensional Insight", "Dodeca (Applied OLAP)",
          "Domo", "Dundas", "Entrinsik Informer", "eQ Technologic eQube", "evidanza", "GoodData", "Halo BI",
          "IBM Cognos BI / IBM Cognos Analytics", "IBM Cognos TM1", "iDashboards",
          "Indicee (D&B Cloud Innovation Center)", "InetSoft", "Infor BI (formerly PM10 and MIS Decisionware)",
          "Information Builders WebFOCUS", "Jaspersoft BI (TIBCO)", "Jedox BI Suite (formerly Palo)",
          "JReport (Jinfonet Software)", "Lavastorm Analytics", "Logi Analytics", "Looker",
          "Microsoft Excel (Excel only and Power Pivot)",
          "Microsoft Power BI (released in 2015, not former Excel-based version)",
          "Microsoft SharePoint Server Excel Services (only BI usage, no portal)",
          "Microsoft SQL Server Reporting Services (SSRS)", "MicroStrategy", "Noetix",
          "OpenText Actuate iHub (formerly Actuate BIRT)", "Oracle BI (formerly OBIEE and OBIFS)",
          "Oracle Hyperion Planning", "Oracle Hyperion Smart View for Office", "Pentaho Business Analytics (HDS)",
          "Phocas", "prevero prevero", "prevero Professional Planner (formerly Winterheller)", "Prognoz Platform",
          "Prophix", "Pyramid Analytics", "Qlik Qlik Sense", "Qlik QlikView", "Salient",
          "SAP Business Explorer (BEx) (including SAP Web Application Designer (WAD) and SAP BEx Analyzer)",
          "SAP BusinessObjects Analysis (OLAP or Office)", "SAP BusinessObjects Design Studio",
          "SAP BusinessObjects Web Intelligence", "SAP BW Integrated Planning (IP)", "SAP Crystal Reports",
          "SAP Lumira", "SAP Predictive Analytics", "SAS Base",
          "SAS Intelligence Platform (incl. SAS Enterprise BI Server)", "SAS Visual Analytics and Visual Statistics",
          "Sisense", "SpagoBI", "Strategy Companion Analyzer", "Tableau", "Tagetik", "TARGIT BI Suite",
          "TIBCO Spotfire", "Yellowfin", "Zoho reports"];

      //$scope.myCategory = $scope.categories[0];
      /*$scope.changeDropDown = function () {
          $scope.calcAndShowResults();
      };*/


        //Resize Cart function-Not working properly
      $scope.resizeChart = function(chart, data,options) {
          chart.draw(data, options);
      };


      //FEEDBACK CALCULATIONS
      $scope.calculateFeedBack = function(results) {
          $scope.feedBack1 = 0;
          $scope.feedBack2 = 0;
          $scope.feedBack3 = [];
          results.forEach(function (res) {
              $scope.feedBack1 += res.feedBack1;
              $scope.feedBack2 += res.feedBack2;
              $scope.feedBack3.push(res.feedBack3);

          });
          $scope.feedBack1 = ($scope.feedBack1 / results.length*100).toFixed(0);
          $scope.feedBack2 = ($scope.feedBack2 / results.length*100).toFixed(0);
          $scope.feedBackLength = results.length;

      };

      // Berechnungen, auch Gültig für Child-state personalResults
      $scope.calculateToolScores = function(results) {
          var scores = {};
          results.forEach(function (res) {
              if ((res.tool !== "") && (res.tool !== null)) {
                  var usageKey = res.tool + " Usage";
                  if (!scores[res.tool]) {
                      scores[res.tool] = {
                          "toolName": res.tool,
                          "average": res.overall,
                          "benefits": res.benefit,
                          "easeOfUse": res.easeOfuse,
                          "usageIntention": res.usage,
                          "usefulness": res.usefulness,
                          "AdvancedVisualization": res.AdvancedVisualization,
                          "BusinessQuery": res.BusinessQuery,
                          "Calculations": res.Calculations,
                          "Collaboration": res.Collaboration,
                          "Dashboards": res.Dashboards,
                          "Drilling": res.Drilling,
                          "ETL": res.ETL,
                          "InMemoryAnalysis": res.InMemoryAnalysis,
                          "InteractiveReports": res.InteractiveReports,
                          "MobileBI": res.MobileBI,
                          "PredictiveAnalysis": res.PredictiveAnalysis,
                          "ScheduledReporting": res.ScheduledReporting,
                          "Spreadsheet": res.Spreadsheet,
                          "StatisticalMethods": res.StatisticalMethods,
                          "VisualDataDiscovery": res.VisualDataDiscovery,
                          "count": 1
                      };
                  } else {
                      var sum = scores[res.tool]["average"] * scores[res.tool]["count"];
                      var benefits = scores[res.tool]["benefits"] * scores[res.tool]["count"];
                      var easeOfUse = scores[res.tool]["easeOfUse"] * scores[res.tool]["count"];
                      var usefulness = scores[res.tool]["usefulness"] * scores[res.tool]["count"];
                      var usageIntention = scores[res.tool]["usageIntention"] * scores[res.tool]["count"];
                      var BusinessQuery = scores[res.tool]["BusinessQuery"] * scores[res.tool]["count"];
                      var Calculations = scores[res.tool]["Calculations"] * scores[res.tool]["count"];
                      var Collaboration = scores[res.tool]["Collaboration"] * scores[res.tool]["count"];
                      var Dashboards = scores[res.tool]["Dashboards"] * scores[res.tool]["count"];
                      var Drilling = scores[res.tool]["Drilling"] * scores[res.tool]["count"];
                      var ETL = scores[res.tool]["ETL"] * scores[res.tool]["count"];
                      var InMemoryAnalysis = scores[res.tool]["InMemoryAnalysis"] * scores[res.tool]["count"];
                      var InteractiveReports = scores[res.tool]["InteractiveReports"] * scores[res.tool]["count"];
                      var MobileBI = scores[res.tool]["MobileBI"] * scores[res.tool]["count"];
                      var PredictiveAnalysis = scores[res.tool]["PredictiveAnalysis"] * scores[res.tool]["count"];
                      var Spreadsheet = scores[res.tool]["Spreadsheet"] * scores[res.tool]["count"];
                      var VisualDataDiscovery = scores[res.tool]["VisualDataDiscovery"] * scores[res.tool]["count"];
                      var ScheduledReporting = scores[res.tool]["ScheduledReporting"] * scores[res.tool]["count"];
                      var StatisticalMethods = scores[res.tool]["StatisticalMethods"] * scores[res.tool]["count"];

                      var newSum = sum + res.overall;
                      var newBenefits = benefits + res.benefit;
                      var newEaseOfUse = easeOfUse + res.easeOfuse;
                      var newUsefulness = usefulness + res.usefulness;
                      var newUsageIntention = usageIntention + res.usage;
                      var newBusinessQuery = BusinessQuery + res.BusinessQuery;
                      var newCalculations = Calculations + res.Calculations;
                      var newCollaboration = Collaboration + res.Collaboration;
                      var newDashboards = Dashboards + res.Dashboards;
                      var newDrilling = Drilling + res.Drilling;
                      var newETL = ETL + res.ETL;
                      var newInMemoryAnalysis = InMemoryAnalysis + res.InMemoryAnalysis;
                      var newInteractiveReports = InteractiveReports + res.InteractiveReports;
                      var newMobileBI = MobileBI + res.MobileBI;
                      var newPredictiveAnalysis = PredictiveAnalysis + res.PredictiveAnalysis;
                      var newSpreadsheet = Spreadsheet + res.Spreadsheet;
                      var newVisualDataDiscovery = VisualDataDiscovery + res.VisualDataDiscovery;
                      var newStatisticalMethods = VisualDataDiscovery + res.StatisticalMethods;
                      var newScheduledReporting = VisualDataDiscovery + res.ScheduledReporting;


                      scores[res.tool]["count"] += 1;
                      scores[res.tool]["average"] = (newSum / (scores[res.tool]["count"]));
                      scores[res.tool]["benefits"] = (newBenefits / (scores[res.tool]["count"]));
                      scores[res.tool]["easeOfUse"] = (newEaseOfUse / (scores[res.tool]["count"]));
                      scores[res.tool]["usefulness"] = (newUsefulness / (scores[res.tool]["count"]));
                      scores[res.tool]["usageIntention"] = (newUsageIntention / (scores[res.tool]["count"]));
                      scores[res.tool]["BusinessQuery"] = (newBusinessQuery / (scores[res.tool]["count"]));
                      scores[res.tool]["Calculations"] = (newCalculations / (scores[res.tool]["count"]));
                      scores[res.tool]["Dashboards"] = (newDashboards / (scores[res.tool]["count"]));
                      scores[res.tool]["Drilling"] = (newDrilling / (scores[res.tool]["count"]));
                      scores[res.tool]["Collaboration"] = (newCollaboration / (scores[res.tool]["count"]));
                      scores[res.tool]["ETL"] = (newETL / (scores[res.tool]["count"]));
                      scores[res.tool]["InMemoryAnalysis"] = (newInMemoryAnalysis / (scores[res.tool]["count"]));
                      scores[res.tool]["InteractiveReports"] = (newInteractiveReports / (scores[res.tool]["count"]));
                      scores[res.tool]["MobileBI"] = (newMobileBI / (scores[res.tool]["count"]));
                      scores[res.tool]["PredictiveAnalysis"] = (newPredictiveAnalysis / (scores[res.tool]["count"]));
                      scores[res.tool]["Spreadsheet"] = (newSpreadsheet / (scores[res.tool]["count"]));
                      scores[res.tool]["VisualDataDiscovery"] = (newVisualDataDiscovery / (scores[res.tool]["count"]));
                      scores[res.tool]["ScheduledReporting"] = (newScheduledReporting / (scores[res.tool]["count"]));
                      scores[res.tool]["StatisticalMethods"] = (newStatisticalMethods / (scores[res.tool]["count"]));

                  }
              }
          });
          return scores;
      }

      //Funktion zu Berechnung der Gesamtdurchscnitte fürs Gesamtergebnis
      $scope.averages = function () {
          $scope.length = 0;
          for (var key in $scope.toolScores) {
              $scope.length+=$scope.toolScores[key].count;
          }

          //Berechnen des AVG über alle Tools OVERALL
          $scope.overallAverage = 0;

          for (var key in $scope.allResults) {
              $scope.overallAverage += $scope.allResults[key].overall;
          }
          $scope.overallAverage = $scope.overallAverage /$scope.length;

          //Berechnen des AVG über alle Tools USEFULNESS


          $scope.usefulnessAverage = 0;

          for (var key in $scope.allResults) {
              $scope.usefulnessAverage += $scope.allResults[key].usefulness;
          }
          $scope.usefulnessAverage = $scope.usefulnessAverage / $scope.length;

          //Berechnen des AVG über alle Tools EASEOFUSE

          $scope.easeOfUseAverage = 0;

          for (var key in $scope.allResults) {
              $scope.easeOfUseAverage += $scope.allResults[key].easeOfuse;
          }
          $scope.easeOfUseAverage = $scope.easeOfUseAverage / $scope.length;

          $scope.benefitsAverage = 0;

          for (var key in $scope.allResults) {
              $scope.benefitsAverage += $scope.allResults[key].benefit;
          }
          $scope.benefitsAverage = $scope.benefitsAverage / $scope.length;

          $scope.usageAverage = 0;

          for (var key in $scope.allResults) {
              $scope.usageAverage += $scope.allResults[key].usage;
          }
          $scope.usageAverage = $scope.usageAverage / $scope.length;
      };

      // ALLE ERGEBNISSE
      $scope.calcAndShowResults = function () {
          databaseService.getAllResponses().then(function (res) {
              $scope.allResults = res.data;
              console.log("all Results so far", $scope.allResults);
              $scope.toolScores = $scope.calculateToolScores($scope.allResults);

              //Create list of all used tools to display in the results dashboard
              if ($scope.myTools === undefined) {
                  $scope.myTools = ["All"];
                  var sortTools = [];
                  for (var key in $scope.toolScores) {
                      sortTools.push([
                          $scope.toolScores[key].toolName]);
                  }
                  //sort Tools alphabetically
                  sortTools.sort(function (a, b) {
                      return (a < b) ? -1 : (a > b) ? 1 : 0;
                  });

                  for (var key in sortTools) {
                      $scope.myTools.push(
                          sortTools[key]);
                  }
              }
              console.log('myToolsList', $scope.myTools);


              $scope.averages();

              $scope.feedBackScores = $scope.allResults.filter(function (res) {
                  return res.alias === "feedback";
              });
              $scope.calculateFeedBack($scope.feedBackScores);

              console.log('myTool', $scope.myTool[0]);


              // To limit the results to a specific Tool based on selection in Feature Chart
              if ($scope.myTool === "All") {
                  $scope.featureScores = $scope.toolScores;
                  $scope.toolResults = $scope.allResults;
                  console.log('Feature Scores', $scope.featureScores);
              }
              else {
                  $scope.toolResults = $scope.allResults.filter(function (res) {
                      return res.tool === $scope.myTool[0];
                  });
                  console.log('toolResults', $scope.toolResults);

                  $scope.featureScores = $scope.calculateToolScores($scope.toolResults);

              }

              console.log('FeatureSCores', $scope.featureScores);



              //Focus for the tabs in the result dashboard
/*              if ($scope.chosenView === 'generalOverall'){
                  document.getElementById("scoretab").focus();
              }
              if ($scope.chosenView === 'generalDistribution'){
                  document.getElementById("disttab").focus();
              }
              if ($scope.chosenView === 'generalFeatures'){
                  document.getElementById("featuretab").focus();
              }
              if ($scope.chosenView === 'generalEaseVsUse'){
                  document.getElementById("easytab").focus();
              }*/



              //Erstellen eines Arrays zum Sortieren mit allen Metriken
              var sortable = [];
              for (var key in $scope.toolScores) {
                  sortable.push([$scope.toolScores[key].toolName, $scope.toolScores[key].usefulness, $scope.toolScores[key].easeOfUse,
                      $scope.toolScores[key].benefits, $scope.toolScores[key].usageIntention, $scope.toolScores[key].average,
                      $scope.toolScores[key].count]);


              }


              //Variable für Bar Chart mit Tools nach Overall Score
              var overallBarChartData = [
                  ['Tool', 'Overall Rating', {role: "style"}]
              ];
              var typenumber = 5;
              var selectedAverage = $scope.overallAverage;
              switch ($scope.myCategory) {
                  case 'Overall': {
                      typenumber = 5;
                      selectedAverage = $scope.overallAverage;
                  } break;
                  case 'Usefulness': {
                      typenumber = 1;
                      selectedAverage = $scope.usefulnessAverage;
                  } break;
                  case 'Ease Of Use': {
                      typenumber = 2;
                      selectedAverage = $scope.easeOfUseAverage;
                  } break;
                  case'Perceived Benefits': {
                      typenumber = 3;
                      selectedAverage = $scope.benefitsAverage;
                  }break;
                  case 'Intention to Use': {
                      typenumber = 4;
                      selectedAverage = $scope.usageAverage;
                  }
              }

              var minWindow = Math.min([Math.min(9, sortable.length - 1)][typenumber], selectedAverage) * 100 - 5;


              //sortieren das Array nach Average
              sortable.sort(function (a, b) {
                  return b[typenumber] - a[typenumber]
              });
              console.log('typeNumber', sortable[typenumber]);

              //Beste 5 Tools in das Array fürs Chart pushen
              for (var i = 0; i <= Math.min(1000, sortable.length - 1); i++) {
                  overallBarChartData.push([sortable[i][0], parseFloat((sortable[i][typenumber] * 100).toFixed(1)), "#2F9682"]);
              }

              overallBarChartData.push(['Average', parseFloat((selectedAverage * 100).toFixed(1)), "#0080FF"]);

              overallBarChartData.sort(function (a, b) {
                  return b[1] - a[1]
              });

              //#####Bar Chart OVERALL######
              google.charts.load('current', {packages: ['corechart', 'bar']});
              google.charts.load('current', {packages: ['bar']});
              google.charts.setOnLoadCallback(drawStuff);

              function drawStuff() {
                  var data = google.visualization.arrayToDataTable(overallBarChartData);

                  var view = new google.visualization.DataView(data);
                  view.setColumns([0, 1,
                      {
                          calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation"
                      },
                      2]);

                  var options = {
                      title: 'Top 10 BI Tools',
                      width: '100%',
                      minWidth: '500px',
                      fontSize: 14,
                      chartArea: {width: '40%', height: '100%'},
                      height: 300 / 10 * Math.min(1000, sortable.length - 1),
                      bar: {groupWidth: "80%"},
                      legend: {position: "none"},
                      tooltip: {textStyle:{
                          fontName: 'Calibri',
                          fontSize: 14
                      }},
                      hAxis: {
                          title: 'Overall Rating',
                          gridlines: {color: 'white', count: 5},
                          viewWindow: {
                              min: sortable[Math.min(1000, sortable.length - 1)][typenumber] * 100 - 10,
                              max: 'auto'
                          }
                      },
                      vAxis: {
                          textStyle:{
                              fontName: 'Calibri',
                              fontSize: 14
                          }

                      }

                  };
                  var chart = new google.visualization.BarChart(document.getElementById("overallBarChart"));
                  chart.draw(view, options);
                  //create trigger to resizeEnd event
              }




              //#######Bubble ChartEase of Use vs Usefulness########

              sortable.sort(function (a, b) {
                  return b[1] - a[1]
              });


              var bubbleXMin = sortable[sortable.length - 1][1];
              var bubbleXMax = sortable[0][1];

              sortable.sort(function (a, b) {
                  return b[2] - a[2]
              });

              var bubbleYMin = sortable[sortable.length - 1][2];
              var bubbleYMax = sortable[0][2];

              var bubbleChartData = [
                  ['ID', 'Usefulness', 'Ease of Use', 'Tool', 'n']
              ];
              for (var key in $scope.toolScores) {
                  bubbleChartData.push(['', $scope.toolScores[key].usefulness * 100, $scope.toolScores[key].easeOfUse * 100,
                      $scope.toolScores[key].toolName, $scope.toolScores[key].count]);
              }
              google.charts.load('current', {'packages': ['corechart']});
              google.charts.setOnLoadCallback(drawSeriesChart);

              function drawSeriesChart() {

                  var data = google.visualization.arrayToDataTable(bubbleChartData);

                  var options = {
                      hAxis: {
                          title: 'Usefulness',
                          viewWindow: {
                              min: bubbleXMin * 100 - 5,
                              max: bubbleXMax * 100 + 5
                          }
                      },
                      vAxis: {
                          title: 'Ease of Use',
                          viewWindow: {
                              min: bubbleYMin * 100 - 5,
                              max: bubbleYMax * 100 + 5
                          }
                      },
                      bubble: {textStyle: {fontSize: 11}},
                      chartArea: {left: '8%', top: '0%', bottom: '10%', width: '60%'},
                      width: '100%',
                      explorer:{},
                      height: 400
                  };

                  var chart = new google.visualization.BubbleChart(document.getElementById('bubble'));
                  chart.draw(data, options);
              }


              //########Use vs Overall##########

              /*sortable.sort(function (a, b) {
                  return b[6] - a[6]
              });


              var XMin = sortable[sortable.length - 1][1];
              var XMax = sortable[0][6];

              sortable.sort(function (a, b) {
                  return b[2] - a[2]
              });

              var YMin = sortable[sortable.length - 1][2];
              var YMax = sortable[0][2];

              var ChartData = [
                  ['ID', '# of users', 'Rating', 'Tool', 'n']
              ];
              for (var key in $scope.toolScores) {
                  ChartData.push(['', $scope.toolScores[key].count, $scope.toolScores[key].average * 100,
                      $scope.toolScores[key].toolName, $scope.toolScores[key].count]);
              }
              google.charts.load('current', {'packages': ['corechart']});
              google.charts.setOnLoadCallback(drawUsevsOverall);

              function drawUsevsOverall() {

                  var data = google.visualization.arrayToDataTable(ChartData);

                  var options = {
                      hAxis: {
                          title: '# of users',
                          viewWindow: {
                              min: XMin  - 5,
                              max: XMax + 5
                          }
                      },
                      vAxis: {
                          title: 'Rating',
                          viewWindow: {
                              min: YMin*100 -5,
                              max: YMax * 100 + 5
                          }
                      },
                      sizeAxis: {
                        minValue: 10000,  maxSize: 20
                      },
                      bubble: {textStyle: {fontSize: 11}},
                      chartArea: {left: '8%', top: '0%', bottom: '10%', width: '60%'},
                      width: '100%',
                      explorer:{},
                      height: 400
                  };

                  var chart = new google.visualization.BubbleChart(document.getElementById('useVsOverall'));
                  chart.draw(data, options);
              }*/




              // #########Donut Chart Example##############
              sortable.sort(function (a, b) {
                  return b[6] - a[6]
              });

              var overviewChartData = [
                  ['Tool Name', 'n']
              ];
              for (var key in sortable) {
                  overviewChartData.push([sortable[key][0], sortable[key][6]]);
              }
              google.charts.load("current", {packages: ["corechart"]});
              google.charts.setOnLoadCallback(drawChart);
              function drawChart() {
                  var data = google.visualization.arrayToDataTable(overviewChartData);
                  var options = {
                      title: 'Tool Usage Distribution',
                      chartArea: {width: '80%', height: '100%'},
                      pieHole: 0.8,
                      sliceVisibilityThreshold: .045,
                      is3D: true,
                      height:350
                  };
                  var chart = new google.visualization.PieChart(document.getElementById('overview'));

                  function resizeChart() {
                      chart.draw(data, options);
                  }

                  if (document.addEventListener) {
                      window.addEventListener('resize', resizeChart);
                  }
                  else if (document.attachEvent) {
                      window.attachEvent('onresize', resizeChart);
                  }
                  else {
                      window.resize = resizeChart;
                  }
                  chart.draw(data, options);
              }


              //#############    Chart for Feature usage for specific tools    ##############



                  var featureAverageData = [['Feature', 'Average', {role: "style"}]];
                  var featureAverage = [];
                  $scope.features.forEach(function (feature) {
                      var avg = 0;
                      for (var key in $scope.featureScores) {
                          avg += $scope.featureScores[key][feature] * $scope.featureScores[key].count;
                      }
                      avg = avg / $scope.toolResults.length;
                      featureAverage.push([feature, parseFloat(avg.toFixed(3) * 100), "#2F9682"]);

                  });


                  featureAverage.sort(function (a, b) {
                      return b[1] - a[1]
                  });
                  featureAverage.forEach(function (el) {
                      featureAverageData.push(el);
                  });

                  google.charts.load('current', {packages: ['corechart', 'bar']});
                  google.charts.load('current', {packages: ['bar']});
                  google.charts.setOnLoadCallback(drawOverallFeature);

                  function drawOverallFeature() {
                      var data = new google.visualization.arrayToDataTable(featureAverageData);
                      var view = new google.visualization.DataView(data);
                      view.setColumns([0, 1,
                          {
                              calc: "stringify",
                              sourceColumn: 1,
                              type: "string",
                              role: "annotation"
                          },
                          2]);

                      var options = {
                          title: 'Top 10 BI Tools',
                          width: '60%',
                          chartArea: {width: '60%', height: '100%'},
                          height: 350,
                          bar: {groupWidth: "80%"},
                          legend: {position: "none"},
                          hAxis: {
                              title: 'Overall Rating',
                              gridlines: {color: 'white', count: 5},
                              viewWindow: {
                                  min: featureAverage[featureAverage.length - 1][1] - 5,
                                  max: 'auto'
                              }
                          },
                          haxis: {}
                      };
                      var chart = new google.visualization.BarChart(document.getElementById('overallFeatureChart'));
                      chart.draw(view, options);

                      $scope.resizeChart (chart, view, options);
                  }









              var toolByFeatureData = [['Tool', 'FeatureScore', {role: "style"}]];
              var toolByFeature = [];
              var activeFeature = $scope.myFeature;

                  for (var key in $scope.toolScores) {

                      toolByFeature.push([$scope.toolScores[key].toolName, parseFloat(($scope.toolScores[key][activeFeature]).toFixed(3) * 100), "#2F9682"]);
                  }



              toolByFeature.sort(function (a, b) {
                  return b[1] - a[1]
              });
              toolByFeature.forEach(function (el) {
                  toolByFeatureData.push(el);
              });

              google.charts.load('current', {packages: ['corechart', 'bar']});
              google.charts.load('current', {packages: ['bar']});
              google.charts.setOnLoadCallback(drawtoolByFeature);

              function drawtoolByFeature() {
                  var data = new google.visualization.arrayToDataTable(toolByFeatureData);
                  var view = new google.visualization.DataView(data);
                  view.setColumns([0, 1,
                      {
                          calc: "stringify",
                          sourceColumn: 1,
                          type: "string",
                          role: "annotation"
                      },
                      2]);

                  var options = {
                      title: 'Top 10 BI Tools',
                      width: '60%',
                      chartArea: {width: '60%', height: '100%'},
                      height: 350,
                      bar: {groupWidth: "80%"},
                      legend: {position: "none"},
                      hAxis: {
                          title: 'Overall Rating',
                          gridlines: {color: 'white', count: 5},
                          viewWindow: {
                              min: toolByFeature[toolByFeature.length - 1][1]-5,
                              max: 'auto'
                          }
                      },
                      haxis: {}
                  };
                  var chart = new google.visualization.BarChart(document.getElementById('toolByFeatureChart'));
                  chart.draw(view, options);
              }
          });
      };

      // Show personal results
      $scope.showPersonalResultFor = function(alias) {
          if (alias !== undefined) {
              $scope.calcAndShowResults();
              console.log(alias)
              $state.go("results.personal", {"alias": alias});
          }
      };

      // initialisierung
      $scope.calcAndShowResults();


  }]);